from typing import List

from attr import dataclass
from selenium.webdriver.chrome.webdriver import WebDriver
from undetected_chromedriver.webelement import UCWebElement


@dataclass
class ChromeProfile(object):
    # db values
    profile_id: str
    profile_name: str
    phone_number: str
    twitter_user_name: str
    proxy: str
    wallet: str
    gmail_address: str
    user_agent: str
    status: str

    # custom value
    selected = False
    path = ""
    is_custom_details = False
    entry_type: str = ""
    data_track_event = ""
    urls: List[str] = list()
    url = ""
    current_index = 0
    x_position = 0
    y_position = 0
    chrome_width = 0
    chrome_height = 0
    driver: WebDriver = None
    chrome_size = {}
    visible_task_count = 0
    root_element: UCWebElement = None
    contestant_id = 0
    secret_code = 0
    total_completed_remaining_event = 0
    total_event = 0
    total_remaining_event = 0
    total_completed_event = 0
    social_url = ""
