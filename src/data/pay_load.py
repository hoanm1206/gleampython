class EDS:
    eds = {}


class AFD:
    afd = {}


class EFD:
    efd = {}


class DBG:
    eds: EDS
    afd: AFD
    efd: EFD
    car = True


class DBGE:
    eed: str
    hed: str
    hedr: str
    csefr: str
    csefn: str
    ae: str
    aebps: str
    re: str


class DETAIL:
    twitter_username: str


class GleamPayload:
    h: str
    challenge_response: str
    use_hcaptcha = True
    dbg: DBG
    dbge: DBGE
    f: str
    rid = False
    details: DETAIL


class GleamPayloadCustomAction:
    h: str
    challenge_response: str
    use_hcaptcha = True
    dbg: DBG
    dbge: DBGE
    f: str
    rid = False
    details: str


class GleamPayloadNoneDetails:
    h: str
    challenge_response: str
    use_hcaptcha = True
    dbg: DBG
    dbge: DBGE
    f: str
    rid = False
    details: str


class ENTRY:
    def __init__(self, twitter_username):
        self.twitter_username = twitter_username


import json

if __name__ == '__main__':
    dbg = DBG()
    eds = EDS()
    dbg.eds = {"entry_method_id": ENTRY("twitter_name").__dict__}
    efd = EFD()
    dbg.efd = {"entry_method_id": ENTRY("twitter_name").__dict__}
    dbg.afd = {}
    dbg.car = True
    dbge = DBGE()
    dbge.eed = "7"
    dbge.hed = "#" + "entry_method_id" + ":undefined:undefined:undefined"
    dbge.hedr = "#" + "entry_method_id" + ":undefined:undefined:undefined"
    dbge.csefr = "rFalse"
    dbge.csefn = "#undefined:false"
    dbge.re = "sed"
    detail = DETAIL()
    detail.twitter_username = "twitter_name"

    gleam_payload = GleamPayload()
    gleam_payload.dbg = dbg.__dict__
    gleam_payload.dbge = dbge.__dict__
    gleam_payload.challenge_response = None
    gleam_payload.details = detail.__dict__
    gleam_payload.h = "hash h value"
    gleam_payload.f = "hash f value"
    gleam_payload.use_hcaptcha = False
    gleam_payload.rid = False

    print(json.dumps(gleam_payload.__dict__))
