class ContestantItem:
    first_name: str
    last_name: str
    email: str
    name: str
    competition_subscription: str
    date_of_birth: str
    stored_dob: str
    send_confirmation: str
    bep20_wallet_address: str
    erc20_address: str
    twitter_account: str
    telegram_account: str
    email_verfication_required = False


class Contestant:
    campaign_key: str
    contestant: ContestantItem
    additional_details: bool
