import sqlite3

from src.data.profile import ChromeProfile


def __connect_to_database():
    conn = sqlite3.connect(database="D:\\Project\\PyCharm\\MyProject\\UndetectedChrome\\src\\repository\\local\\ChromeProfileDB.db")
    return conn


def get_list_profile_chrome():
    conn = __connect_to_database()
    with open('D:\\Project\\PyCharm\\MyProject\\UndetectedChrome\\src\\repository\\local\\dump.txt', 'r') as f:
        dump = f.read()
        conn.executescript(dump)
        conn.commit()
    cursor = conn.cursor()
    query = "Select * from ProfileChrome"
    cursor.execute(query)
    list_profile = list()
    for row in cursor:
        profile = ChromeProfile(*row)
        print(profile)
        list_profile.append(profile)
    conn.close()
    return list_profile




if __name__ == '__main__':
    get_list_profile_chrome()
