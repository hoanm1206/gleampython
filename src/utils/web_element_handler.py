import time

from selenium.webdriver.chrome.webdriver import *
from selenium.webdriver.common.alert import *
from selenium.webdriver.support.expected_conditions import *
from selenium.webdriver.support.ui import *
from undetected_chromedriver.webelement import UCWebElement

from src.data.profile import ChromeProfile


class WebElementHandler:

    def __init__(self, driver: WebDriver):
        self.driver_wait = WebDriverWait(driver, timeout=30)
        self.driver = driver

    def get_driver(self):
        return self.driver

    def terminate_driver_wait(self):
        self.driver_wait._timeout = 0

    def execute_script(self, json: str, *args):
        return self.driver.execute_script(json)

    def try_click_button(self, element: UCWebElement):
        if element is not None:
            try:
                element.click_safe()
            except:
                print("failed to click element")
            else:
                return True
        return False

    def switch_to_main_tab(self):
        try:
            self.driver.switch_to.window(self.driver.window_handles[0])
        except:
            time.sleep(1)
            try:
                self.driver.switch_to.window(self.driver.window_handles[0])
            except:
                print("failed to switch to main tab")

    def find_element(self, by, value, root_element=None):
        element = None
        try:
            self.wait_for_element_enable(by, value, root_element)
            if root_element is not None:
                element = root_element.find_element(by, value)
            else:
                element = self.driver.find_element(by, value)
        except WebDriverException:
            print("elements not found")
        return element

    def find_element_fast(self, by, value, root_element=None):
        element = None
        try:
            self.wait_for_page_load_complete()
            if root_element is not None:
                element = root_element.find_element(by, value)
            else:
                element = self.driver.find_element(by, value)
        except WebDriverException:
            print("elements not found")
        return element

    def get_cookies(self):
        cookies = self.driver.get_cookies()
        str_cookie = ""
        for cookie in cookies:
            name = cookie.get("name")
            value = cookie.get("value")
            is_last = False if name is not cookies[len(cookies) - 1].get("name") else True
            str_cookie += str(name) + "=" + str(value) + ("" if is_last else ";")
        return str_cookie

    def switch_to_new_tab(self):
        self.driver.switch_to.new_window("tab")
        self.driver.switch_to.window(self.driver.window_handles[1])

    def find_elements(self, by, value, root_element=None, should_wait=True):
        elements = list()
        try:
            if should_wait:
                self.wait_for_element_enable(by, value, root_element)
            if root_element is not None:
                elements = root_element.find_elements(by, value)
            else:
                elements = self.driver.find_elements(by, value)
        except WebDriverException:
            print("elements not found")
        return elements

    def wait_for_element_enable(self, by, value, root_element=None):
        time_out = 30
        current_time = 0
        element_find = False
        while current_time < time_out and element_find == False:
            try:
                if root_element is not None:
                    root_element.find_element(by, value).is_enabled()
                else:
                    self.driver.find_element(by, value).is_enabled()
                print("find element successfully")
                return
            except:
                print("finding element: " + str(current_time) + " time")
                time.sleep(1)
                current_time += 1

    def wait_for_elements_visible(self, by):
        list_element = list()
        try:
            # self.wait_for_alert_disappear()  # wait until alert close or time out: 10 sec
            list_element = (self.driver_wait.until(presence_of_all_elements_located(by)))
        except WebDriverException:
            print("some thing wrong")
        else:
            print("list element: " + str(list_element))
        finally:
            print("TODO : notify about status, error,... using observable")
        return list_element

    def wait_for_element_visible(self, by):
        element = None
        try:
            self.wait_for_alert_disappear()  # wait until alert close or time out: 10 sec
            element = self.driver_wait.until(visibility_of_element_located(by))
        except Exception as e:
            print("some thing wrong: " + str(e))
        else:
            print("element: " + str(element))
        finally:
            print("TODO : notify about status, error,... using observable")
        return element

    def wait_for_child_element_visible(self, root_element: UCWebElement, by):
        child_element = None
        end_time = time.time() + 20
        while time.time() < end_time and child_element is None:  # get element in 20 second
            try:
                child_element = root_element.find_element(by)
            except WebDriverException:
                print("can not find child element")
            else:
                print("child element: " + str(child_element))
        return child_element

    def get_element_if_exist(self, by):
        try:
            element = self.driver.find_element(by)
            return element
        except WebDriverException:
            print("element not found")
        return None

    def wait_for_page_load_complete(self):
        time_out = 30
        current_time = 0
        while current_time < time_out and self.driver.execute_script('return document.readyState') == 'complete':
            time.sleep(1)
            current_time += 1
        time.sleep(1)

    def open_new_tab_flow(self, profile: ChromeProfile):
        self.switch_to_new_tab()
        self.driver.get(profile.social_url)
        time.sleep(3)
        # self.wait_for_page_load_complete()

    def close_new_tab_and_back_to_main_task(self):
        time.sleep(2)
        self.driver.switch_to.window(self.driver.window_handles[0])
        self.driver.close()
        self.switch_to_main_tab()
        time.sleep(1)

    def wait_for_alert_disappear(self):
        is_showing = True
        start_time = round(time.time())

        def is_alert_showing():
            timeout_seconds = 10  # Set the timeout to 10 seconds
            nonlocal start_time
            if not is_showing:
                return False
            else:
                return round(time.time()) - start_time < timeout_seconds

        def check_alert():
            try:
                Alert(self.driver)
                nonlocal is_showing
                is_showing = True
            except WebDriverException:
                is_showing = False

        while is_alert_showing():
            check_alert()
