import os
import shutil
import tempfile

import undetected_chromedriver as uc


class ProxyExtension:
    manifest_json = """
    {
        "version": "1.0.0",
        "manifest_version": 2,
        "name": "Chrome Proxy",
        "permissions": [
            "proxy",
            "tabs",
            "unlimitedStorage",
            "storage",
            "<all_urls>",
            "webRequest",
            "webRequestBlocking"
        ],
        "background": {"scripts": ["background.js"]},
        "minimum_chrome_version": "76.0.0"
    }
    """

    background_js = """
    var config = {
        mode: "fixed_servers",
        rules: {
            singleProxy: {
                scheme: "http",
                host: "%s",
                port: %d
            },
            bypassList: ["localhost"]
        }
    };

    chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

    function callbackFn(details) {
        return {
            authCredentials: {
                username: "%s",
                password: "%s"
            }
        };
    }

    chrome.webRequest.onAuthRequired.addListener(
        callbackFn,
        { urls: ["<all_urls>"] },
        ['blocking']
    );
    """

    def __init__(self, host, port, user, password):
        self._dir = os.path.normpath(tempfile.mkdtemp())

        manifest_file = os.path.join(self._dir, "manifest.json")
        with open(manifest_file, mode="w") as f:
            f.write(self.manifest_json)

        background_js = self.background_js % (host, port, user, password)
        background_file = os.path.join(self._dir, "background.js")
        with open(background_file, mode="w") as f:
            f.write(background_js)

    @property
    def directory(self):
        return self._dir

    def __del__(self):
        shutil.rmtree(self._dir)


if __name__ == "__main__":
    proxy = ("103.176.251.209", 56788, "PVN64338", "zc4qrejV")
    proxy_extension = ProxyExtension(*proxy)
    # This code partially disables WebRTC
    preferences = {
        "webrtc.ip_handling_policy": "disable_non_proxied_udp",
        "webrtc.multiple_routes_enabled": False,
        "webrtc.nonproxied_udp_enabled": False
    }
    options = uc.ChromeOptions()
    options.add_argument(f"--load-extension={proxy_extension.directory}")
    #options.add_experimental_option("prefs", preferences)
    options.add_argument(r'--user-data-dir=D:\Airdrop\Profile\51693304578983232243')
    options.add_argument(r'--profile-directory=Default')

    driver = uc.Chrome(
        version_main=110,
        use_subprocess=True,
        browser_executable_path="D:\\Airdrop\\gologin\\gologin\\browser\\orbita-browser\\chrome.exe",
        options=options)

    driver.get("https://getip.pro/")
    driver.quit()
