import threading

from queue import Queue

jobs = Queue(maxsize=101)


def do_stuff(q):
    while not q.empty():
        items = q.get()
        func = items[0]
        args = items[1]  # user items[1:] when have multiple param
        func(args)
        q.task_done()


def push_task_to_queue(func, task_count):
    # put task as function into queue
    jobs.put(func)
    # for i in range(task_count):
    #     jobs.put(func)


def start_queue():
    # default 6 thread run in parallel
    for i in range(6):
        worker = threading.Thread(target=do_stuff, args=(jobs,))
        worker.start()
        print("starting task: " + str(i))
    jobs.join()
    print("all done")


def terminate_queue():
    global jobs
    jobs = Queue()

# def test(index):
#     print("test :")
#
#
# if __name__ == '__main__':
#     list_value = (test, 1)
#     push_task_to_queue(list_value, 12)
#     start_queue()
