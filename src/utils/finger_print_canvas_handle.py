import os
import shutil
import tempfile

import undetected_chromedriver as uc
class CanvasHtml:
    manifest_json = """
        {
            "update_url": "https://clients2.google.com/service/update2/crx",
            
              "version": "0.2.0",
              "manifest_version": 2,
              "offline_enabled": true,
              "name": "Canvas Fingerprint Defender",
              "permissions": ["storage", "contextMenus", "notifications"],
              "homepage_url": "https://mybrowseraddon.com/canvas-defender.html",
              "description": "Defending against Canvas fingerprinting by reporting a fake value.",
              "background": {
                "persistent": false,
                "scripts": [
                  "lib/config.js", 
                  "lib/chrome.js",
                  "lib/runtime.js",
                  "lib/common.js"
                ]
              },
              "browser_action": {
                "default_popup": "data/popup/popup.html",
                "default_title": "Canvas Fingerprint Defender",
                "default_icon": {
                  "16": "data/icons/16.png",
                  "32": "data/icons/32.png",
                  "48": "data/icons/48.png",
                  "64": "data/icons/64.png"
                }
              },
              "content_scripts": [{
                "all_frames": true,
                "matches": ["*://*/*"],
                "match_about_blank": true,
                "run_at": "document_start",
                "js": ["data/content_script/inject.js"]
              }],
              "icons": {
                "16": "data/icons/16.png",
                "32": "data/icons/32.png",
                "48": "data/icons/48.png",
                "64": "data/icons/64.png",
                "128": "data/icons/128.png"
              }
            }
        """

    background_js = """
           var background = (function () {
  let tmp = {};
  /*  */
  chrome.runtime.onMessage.addListener(function (request) {
    for (let id in tmp) {
      if (tmp[id] && (typeof tmp[id] === "function")) {
        if (request.path === "background-to-page") {
          if (request.method === id) {
            tmp[id](request.data);
          }
        }
      }
    }
  });
  /*  */
  return {
    "receive": function (id, callback) {
      tmp[id] = callback;
    },
    "send": function (id, data) {
      chrome.runtime.sendMessage({
        "method": id, 
        "data": data,
        "path": "page-to-background"
      }, function () {
        return chrome.runtime.lastError;
      });
    }
  }
})();

var inject = function () {
  const getImageData = CanvasRenderingContext2D.prototype.getImageData;
  //
  let noisify = function (canvas, context) {
    if (context) {
      const shift = {
        'r': Math.floor(Math.random() * 10) - 5,
        'g': Math.floor(Math.random() * 10) - 5,
        'b': Math.floor(Math.random() * 10) - 5,
        'a': Math.floor(Math.random() * 10) - 5
      };
      //
      const width = canvas.width;
      const height = canvas.height;
      //
      if (width && height) {
        const imageData = getImageData.apply(context, [0, 0, width, height]);
        //
        for (let i = 0; i < height; i++) {
          for (let j = 0; j < width; j++) {
            const n = ((i * (width * 4)) + (j * 4));
            imageData.data[n + 0] = imageData.data[n + 0] + shift.r;
            imageData.data[n + 1] = imageData.data[n + 1] + shift.g;
            imageData.data[n + 2] = imageData.data[n + 2] + shift.b;
            imageData.data[n + 3] = imageData.data[n + 3] + shift.a;
          }
        }
        //
        window.top.postMessage("canvas-fingerprint-defender-alert", '*');
        context.putImageData(imageData, 0, 0); 
      }
    }
  };
  //
  HTMLCanvasElement.prototype.toBlob = new Proxy(HTMLCanvasElement.prototype.toBlob, {
    apply(target, self, args) {
      noisify(self, self.getContext("2d"));
      //
      return Reflect.apply(target, self, args);
    }
  });
  //
  HTMLCanvasElement.prototype.toDataURL = new Proxy(HTMLCanvasElement.prototype.toDataURL, {
    apply(target, self, args) {
      noisify(self, self.getContext("2d"));
      //
      return Reflect.apply(target, self, args);
    }
  });
  //
  CanvasRenderingContext2D.prototype.getImageData = new Proxy(CanvasRenderingContext2D.prototype.getImageData, {
    apply(target, self, args) {
      noisify(self.canvas, self);
      //
      return Reflect.apply(target, self, args);
    }
  });
  // Note: this variable is for targeting sandboxed iframes
  document.documentElement.dataset.cbscriptallow = true;
};

let script_1 = document.createElement("script");
script_1.textContent = "(" + inject + ")()";
document.documentElement.appendChild(script_1);
script_1.remove();

if (document.documentElement.dataset.cbscriptallow !== "true") {
  let script_2 = document.createElement("script");
  //
  script_2.textContent = `{
    const iframes = [...window.top.document.querySelectorAll("iframe[sandbox]")];
    for (let i = 0; i < iframes.length; i++) {
      if (iframes[i].contentWindow) {
        if (iframes[i].contentWindow.CanvasRenderingContext2D) {
          iframes[i].contentWindow.CanvasRenderingContext2D.prototype.getImageData = CanvasRenderingContext2D.prototype.getImageData;
        }
        //
        if (iframes[i].contentWindow.HTMLCanvasElement) {
          iframes[i].contentWindow.HTMLCanvasElement.prototype.toBlob = HTMLCanvasElement.prototype.toBlob;
          iframes[i].contentWindow.HTMLCanvasElement.prototype.toDataURL = HTMLCanvasElement.prototype.toDataURL;
        }
      }
    }
  }`;
  //
  window.top.document.documentElement.appendChild(script_2);
  script_2.remove();
}

window.addEventListener("message", function (e) {
  if (e.data && e.data === "canvas-fingerprint-defender-alert") {
    background.send("fingerprint", {
      "host": document.location.host
    });
  }
}, false);
        """

    def __init__(self):
        self._dir = os.path.normpath(tempfile.mkdtemp())

        manifest_file = os.path.join(self._dir, "manifest.json")
        with open(manifest_file, mode="w") as f:
            f.write(self.manifest_json)

        background_file = os.path.join(self._dir, "inject.js")
        with open(background_file, mode="w") as f:
            f.write(self.background_js)

    @property
    def directory(self):
        return self._dir

    def __del__(self):
        shutil.rmtree(self._dir)


if __name__ == "__main__":
    canvas = CanvasHtml()
    # This code partially disables WebRTC
    preferences = {
        "webrtc.ip_handling_policy": "disable_non_proxied_udp",
        "webrtc.multiple_routes_enabled": False,
        "webrtc.nonproxied_udp_enabled": False
    }
    options = uc.ChromeOptions()
    options.add_argument(r'--user-data-dir=D:\Airdrop\Profile\51693304578983232243')
    options.add_argument(r'--profile-directory=Default')

    driver = uc.Chrome(
        version_main= 110,
        use_subprocess= True,
        browser_executable_path="D:\\Airdrop\\gologin\\gologin\\browser\\orbita-browser\\chrome.exe",
        options=options)

    driver.get("https://getip.pro/")
    driver.quit()
