import requests
from eth_account import Account
from web3 import Web3


def get_wallet_balance_in_usd():
    # ge balance in wei
    get_balance_api = "https://api.bscscan.com/api?module=account&action=balance&address=0xE64c5b11D9290dEcC9b737C63c066ba6f826Dd4A&apikey=PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK"
    wei_value = requests.get(get_balance_api).json()['result']

    # Get list id for crc:
    # ids_api = 'https://api.coingecko.com/api/v3/asset_platforms'
    # ids = requests.get(ids_api)

    # Get the current ETH-USD exchange rate
    response = requests.get('https://api.coingecko.com/api/v3/simple/price?ids=binancecoin&vs_currencies=usd')
    eth_usd_rate = response.json()['binancecoin']['usd']

    # Calculate the USD value of the Wei
    usd_value = float(wei_value) * eth_usd_rate / 10 ** 18

    print(f'{wei_value} Wei is equivalent to {usd_value:.2f} USD')


def get_all_token_balance_greater_than_0_in_wallet():
    api_get_txlist = "https://api.bscscan.com/api?" \
                     "module=account&" \
                     "action=txlist&" \
                     "address=0xE64c5b11D9290dEcC9b737C63c066ba6f826Dd4A&" \
                     "startblock=0&" \
                     "endblock=99999999&" \
                     "page=1&" \
                     "offset=10000&" \
                     "sort=desc&apikey=PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK"
    wallet_address = '0xE64c5b11D9290dEcC9b737C63c066ba6f826Dd4A'
    api_key = 'PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK'
    api_url = f'https://api.bscscan.com/api/v1/balances/{wallet_address}?apiKey={api_key}'

    response = requests.get(api_url)
    if response.status_code == 200:
        tokens = response.json()['result']
        for token in tokens:
            balance = int(token['balance']) / (10 ** int(token['tokenDecimal']))
            if balance > 0:
                print(f"{token['tokenSymbol']}: {balance}")
    else:
        print(f"Error: {response.status_code} - {response.text}")
    # txtlist_response = requests.get(api_get_txlist)


def send_coin():
    # Connect to BSC network
    web3 = Web3(Web3.HTTPProvider('https://bsc-dataseed1.binance.org:443'))

    # Load account using private key
    private_key = '0x...'  # Replace with your private key
    account = Account.privateKeyToAccount(private_key)

    # Get sender address
    sender_address = account.address

    # Set recipient address and amount to send
    recipient_address = '0x...'  # Replace with recipient address
    amount_to_send = web3.toWei('0.1', 'ether')  # Replace with amount to send in BNB

    # Create transaction object
    tx_object = {
        'from': sender_address,
        'to': recipient_address,
        'value': amount_to_send,
        'gasPrice': web3.toHex(web3.toWei('5', 'gwei')),
        'gas': web3.toHex(21000)
    }

    # Sign and send transaction
    signed_tx = account.signTransaction(tx_object)
    tx_hash = web3.eth.sendRawTransaction(signed_tx.rawTransaction)

    # Print transaction hash
    print(f'Transaction hash: {web3.toHex(tx_hash)}')


if __name__ == '__main__':
    get_all_token_balance_greater_than_0_in_wallet()
    print("done")
