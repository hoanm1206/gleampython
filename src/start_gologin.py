import json
import os
import pathlib
import random
import socket
import sys
import tempfile
import socks
import socket
import requests
import undetected_chromedriver as uc


def getRandomPort():
    while True:
        port = random.randint(1000, 35000)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1', port))
        if result == 0:
            continue
        else:
            return port
        sock.close()


class Product(object):
    def __init__(self, options):
        self.tmpdir = options.get('tmpdir', tempfile.gettempdir())
        self.address = options.get('address', '127.0.0.1')
        self.extra_params = options.get('extra_params', [])
        self.port = options.get('port', 3500)  # port mặc định
        self.local = options.get('local', False)
        self.spawn_browser = options.get('spawn_browser', True)
        self.credentials_enable_service = options.get('credentials_enable_service')
        self.user_agent = options.get('user_agent', '')

        home = str(pathlib.Path.home())
        self.executablePath = options.get('executablePath',
                                          'D:\\Airdrop\\gologin-create-profile-tool_6.0.1\\gologin\\browser\\orbita-browser\\chrome.exe')  # Orbita Path
        if not os.path.exists(self.executablePath) and sys.platform == "darwin":
            self.executablePath = os.path.join(home, '.gologin/browser/Orbita-Browser.app/Contents/MacOS/Orbita')
        if self.extra_params:
            print('extra_params', self.extra_params)
        self.setProfileId(options.get('profile_id'))

    def setProfileId(self, profile_id):
        self.profile_id = profile_id
        if self.profile_id == None:
            return
        self.profile_path = os.path.join(self.tmpdir, self.profile_id)

    def getParams(self, my_proxy):
        proxy = ''
        proxy_host = ''
        try:
            schema = my_proxy.split('://')[0]
            if schema == 'socks5':
                proxy = my_proxy
            else:
                proxies = my_proxy.split('://')[1].split(':')
                proxy_host = proxies[0]
                port = proxies[1]
                proxy = f"{proxy_host}:{port}"
        except Exception as re:
            print(re)

        params = [
            self.executablePath,
            '--user-data-dir=' + self.profile_path,
            '--password-store=basic',
            '--lang=en',
            '--user-agent=' + self.user_agent

        ]
        # print(proxy)
        if proxy:
            hr_rules = '"MAP * 0.0.0.0 , EXCLUDE %s"' % (proxy_host)
            params.append('--proxy-server=' + proxy)
            params.append('--host-resolver-rules=' + hr_rules)

        for param in self.extra_params:
            params.append(param)
        return params

    def Change_Proxy(self, proxy):
        schema = host = port = username = password = ''
        if proxy:
            username = ''
            password = ''
            try:
                schema = proxy.split('://')[0]
                host_port = proxy.split('://')[1].split(':')
                host = host_port[0]
                port = host_port[1]
                if len(host_port) == 4:
                    username = host_port[2]
                    password = host_port[3]

                # print(schema)
            except Exception as re:
                print(re)

            with open(f"{self.profile_path}\\Default\\Preferences", 'r', encoding="utf8") as aa:
                data_profile = json.load(aa)
                data_profile['gologin']['proxy'] = {
                    "id": None,
                    "mode": schema,
                    "host": host,
                    "port": port,
                    "username": username,
                    "password": password,
                    "changeIpUrl": str('null'),
                    "autoProxyRegion": "vn",
                    "torProxyRegion": "vn"
                }
                if data_profile:
                    pfile = open(f"{self.profile_path}\\Default\\Preferences", 'w')
                    json.dump(data_profile, pfile)
                    pfile.close()

    def formatProxyUrlPassword(self, proxyzz):
        if proxyzz.get('username', '') == '':
            return proxyzz.get('mode', 'http') + '://' + proxyzz.get('host', '') + ':' + str(
                proxyzz.get('port', 80))
        else:
            return proxyzz.get('mode', 'http') + '://' + proxyzz.get('username', '') + ':' + proxyzz.get(
                'password') + '@' + proxyzz.get('host', '') + ':' + str(proxyzz.get('port', 80))

    def getTimeZone(self, ):
        with open(f"{self.profile_path}\\Default\\Preferences", 'r') as f:
            data_profile = json.load(f)
            modes = data_profile['gologin']['proxy']['mode']
            hosts = data_profile['gologin']['proxy']['host']
            ports = data_profile['gologin']['proxy']['port']
            try:
                username = data_profile['gologin']['proxy']['username']
                password = data_profile['gologin']['proxy']['password']
            except:
                username = ''
                password = ''

            if (modes == "none"):
                proxyzz = None
            elif (modes == "socks5"):
                proxyzz = {
                    'mode': 'socks5h',
                    'host': hosts,
                    'port': ports,
                    'username': username,
                    'password': password
                }
            elif (username == None):
                proxyzz = {
                    'mode': modes,
                    'host': hosts,
                    'port': ports,
                    'username': username,
                    'password': password
                }
            elif (username == 'Null'):
                proxyzz = {
                    'mode': modes,
                    'host': hosts,
                    'port': ports,
                    'username': username,
                    'password': password
                }
            else:
                proxyzz = data_profile['gologin']['proxy']
        # print(proxyzz)
        headers = {
            'User-Agent': 'gologin-api'
        }
        if proxyzz:
            proxies = dict(
                http=self.formatProxyUrlPassword(proxyzz),
                https=self.formatProxyUrlPassword(proxyzz)
            )
            data = requests.get('https://time.gologin.com', proxies=proxies, headers=headers)
        else:
            data = requests.get('https://time.gologin.com')
        return json.loads(data.content.decode('utf-8'))

    def update(self, profile_id):
        self.tz = self.getTimeZone()
        self.ChangeTimezone()

    def ChangeTimezone(self):
        ips = self.tz.get('ip')
        timezons = self.tz.get('timezone')
        # print(timezons)
        try:
            with open(f"{self.profile_path}\\Default\\Preferences", 'r') as q:
                preferences = json.load(q)
                preferences['gologin']['webRTC']['publicIp'] = ips
                preferences['gologin']['webRtc']['publicIP'] = ips
                preferences['gologin']['webRtc']['public_ip'] = ips
                preferences['gologin']['webRtc']['localIps'] = ips
                preferences['gologin']['timezone']['id'] = timezons

                preferences['gologin']['geoLocation']['latitude'] = self.tz.get('ll', [0, 0])[0]
                preferences['gologin']['geoLocation']['longitude'] = self.tz.get('ll', [0, 0])[1]
                preferences['gologin']['geoLocation']['accuracy'] = self.tz.get('accuracy', 0)

                preferences['gologin']['geolocation']['latitude'] = self.tz.get('ll', [0, 0])[0]
                preferences['gologin']['geolocation']['longitude'] = self.tz.get('ll', [0, 0])[1]
                preferences['gologin']['geolocation']['accuracy'] = self.tz.get('accuracy', 0)
                q.close()
                if preferences:
                    pfiles = open(f"{self.profile_path}\\Default\\Preferences", 'w')
                    json.dump(preferences, pfiles)
                    pfiles.close()
        except Exception as e:
            print(e)

    def getlistProfileId(self):
        return os.listdir(self.tmpdir)


# if __name__ == "__main__":
#     starts = Product({
#         "tmpdir": 'D:\\Airdrop\\Profile',  # Đường dẫn folder profile
#         "local": False,
#         "credentials_enable_service": False
#     })
#
#     starts.getlistProfileId()
#
#     Proxy = "http://103.176.251.209:56788:PVN64338:zc4qrejV"
#
#     profile_id = '51693304578983232243'
#     starts.setProfileId(profile_id)
#     if Proxy:
#         starts.Change_Proxy(Proxy)  # Đổi proxy cho profile cần mở. Để cảm bảo vân tay chính xác theo proxy
#         starts.update(profile_id)  # Cập nhật profile
#
#     print('Profile start: ', profile_id)
#     print('Proxy start: ', Proxy)
#     params = starts.getParams(Proxy)  # Kết nối với profile
#
#     chrome_driver_path = 'C:\\Users\\hoanm\\AppData\\Roaming\\undetected_chromedriver\\undetected_chromedriver.exe'
#     browser_path = "D:\\Airdrop\\gologin\\gologin\\browser\\orbita-browser\\chrome.exe"
#     options = uc.ChromeOptions()
#     for param in params:
#         options.add_argument(param)
#     driver = uc.Chrome(
#         version_main=110,
#         use_subprocess=True,
#         driver_executable_path=chrome_driver_path,
#         browser_executable_path=browser_path,
#         options=options)
#
#     driver.get('https://getip.pro/')
#     driver.close()
