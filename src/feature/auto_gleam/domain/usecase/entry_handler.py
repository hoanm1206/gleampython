import time
from pathlib import Path
from typing import List

from undetected_chromedriver.webelement import UCWebElement

from src.feature.auto_gleam.data_track_event.discord import JOIN_DISCORD_SERVER
from src.feature.auto_gleam.data_track_event.other import SHARE_ACTION, SUBSCRIBE_EMAIL
from src.feature.auto_gleam.data_track_event.twitter import *
from src.feature.auto_gleam.domain.events.discord_events import join_discord_server
from src.feature.auto_gleam.domain.events.email_events import subscribe_email
from src.feature.auto_gleam.domain.events.other_events import click_event_author_expired, click_close_entry_button, \
    authentication_social
from src.feature.auto_gleam.domain.events.twitter_events import *
from src.feature.auto_gleam.domain.usecase.request_handler import RequestHandler


class EntryHandler:

    def __init__(self, profile: ChromeProfile, root_elements: List[UCWebElement], events: List[str],
                 web_element_handler: WebElementHandler):
        self.profile = profile
        self.events = events
        self.root_elements = root_elements
        self.web_element_handler = web_element_handler

    def make_events(self):
        for index, value in enumerate(self.root_elements):
            event = self.events[index]
            root_element = value
            request_handler = RequestHandler(self.profile, root_element, self.web_element_handler)
            self.profile.data_track_event = event
            self.profile.is_custom_details = self.is_custom_detail()
            self.profile.entry_type = request_handler.get_entry_type(event)
            self.profile.social_url = self.get_social_url(root_element)
            if self.ignor_event():
                return
            else:
                request_handler.start_request((self.on_event, root_element))
        self.save_result()

    def on_event(self, root_element: UCWebElement, other_event: str = None, should_return: bool = False):
        event = self.profile.data_track_event
        # authen lai các mạng xã hội
        if other_event is not None:
            if other_event == 'error_auth_expired' or other_event == 'error_challenge_failed':
                click_event_author_expired(self.web_element_handler, root_element)
                if len(self.profile.driver.window_handles) > 1:
                    while len(self.profile.driver.window_handles) > 1:
                        time.sleep(1)
                    time.sleep(1)
                else:
                    authentication_social(self.web_element_handler, root_element)
                    while len(self.profile.driver.window_handles) > 1:
                        time.sleep(1)
                    time.sleep(1)
            elif other_event == 'close_entry':
                click_close_entry_button(self.web_element_handler, root_element)  # đóng thẻ entry nếu cần
        if should_return:
            return
        if event == Twitter.TWEET:
            tweet_with_hash_tag(self.profile, self.web_element_handler)
        elif event == Twitter.TWEET_WITH_HASH_TAG:
            add_tweet_hash_tag(self.profile, self.web_element_handler)
        elif event == Twitter.FOLLOW:
            follow_twitter(self.profile, self.web_element_handler)
        elif event == Twitter.RETWEET:
            retweet_twitter(self.profile, self.web_element_handler)
        elif event == JOIN_DISCORD_SERVER:
            join_discord_server(self.profile, self.web_element_handler)
        elif event == SUBSCRIBE_EMAIL:
            subscribe_email(self.web_element_handler, root_element)

    def get_social_url(self, root_element: UCWebElement):
        value = ".//*[@class='expandable']//*[contains(@href,'https://') or contains(@ng-href,'https://')]"
        element = root_element.find_elements(By.XPATH, value)
        if len(element) > 0:
            return element[0].get_attribute("href")
        else:
            return None

    def ignor_event(self):
        return self.profile.data_track_event == SHARE_ACTION

    def is_custom_detail(self):
        return self.profile.data_track_event is not Twitter.FOLLOW \
            and self.profile.data_track_event is not Twitter.RETWEET \
            and self.profile.data_track_event is not Twitter.TWEET \
            and self.profile.data_track_event is not Twitter.TWEET_WITH_HASH_TAG \
            and self.profile.data_track_event is not JOIN_DISCORD_SERVER

    def save_result(self):
        profile_name = 'Profile: ' + str(self.profile.profile_id)
        total_event_completed = str(self.profile.total_completed_event)
        total_event = str(self.profile.total_event) + '\n'
        result = profile_name + ' --- ' + total_event_completed + "/" + total_event
        print("Tổng số events hoàn thành trên toàn bộ events: " + str(
            self.profile.total_completed_event) + "/" + str(
            self.profile.total_event))
        file_path = Path('D:\\Airdrop\\Result.txt')
        file = open(file_path, 'w')
        file.write(result)
        file.close()
