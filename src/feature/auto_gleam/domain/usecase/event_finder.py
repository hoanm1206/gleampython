from typing import List

from selenium.webdriver.common.by import By
from undetected_chromedriver.webelement import UCWebElement

from src.data.profile import ChromeProfile
from src.utils.web_element_handler import WebElementHandler


class EventFinder:

    def __init__(self, profile: ChromeProfile, web_element_handler: WebElementHandler):
        self.profile = profile
        self.web_element_handler = web_element_handler

    def __get_elements_visible__(self):
        value = "//i[@ng-class='tallyIcon(entry_method)' and ((not(@class) or @class='' and not(@class='fas fa-check')))]//ancestor::div[contains(@class,'entry-method')]"
        list_task = self.web_element_handler.find_elements(By.XPATH, value)
        self.profile.visible_task_count = len(list_task)
        return list_task

    def __get_elements_locked__(self):
        value = "//i[@ng-class='tallyIcon(entry_method)' and (@class='fas fa-lock')]//ancestor::div[contains(@class,'entry-method')]"
        list_task = self.web_element_handler.find_elements(By.XPATH, value, should_wait=False)
        return list_task

    def get_all_root_element(self):
        lst1: List[UCWebElement] = self.__get_elements_visible__()
        lst2: List[UCWebElement] = self.__get_elements_locked__()
        lst1.extend(lst2)
        return lst1

    def get_all_event(self):
        value = "//i[@ng-class='tallyIcon(entry_method)']//ancestor::div[contains(@class,'entry-method')]"
        list_task = self.web_element_handler.find_elements(By.XPATH, value, should_wait=False)
        return list_task

    def get_all_completed_event(self):
        value = "//i[@ng-class='tallyIcon(entry_method)' and ((@class) or @class='' and (@class='fas fa-check'))]"
        list_task = self.web_element_handler.find_elements(By.XPATH, value, should_wait=False)
        return list_task

    def get_all_event_remaining(self):
        elements = self.get_all_root_element()
        lst_event = list()
        for root_element in elements:
            event = root_element.find_element(By.XPATH, ".//a[1]").get_attribute("data-track-event")
            lst_event.append(event)
        total_completed_event = self.get_all_completed_event()
        all_event_count = self.get_all_event()
        self.profile.total_event = len(all_event_count)
        self.profile.total_remaining_event = len(lst_event)
        self.profile.total_completed_event = len(total_completed_event)
        return lst_event
