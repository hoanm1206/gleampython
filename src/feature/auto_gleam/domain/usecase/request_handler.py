import json
import re
from pathlib import Path

import requests
from selenium.webdriver.common.by import By
from undetected_chromedriver.webelement import UCWebElement

from src.data.login_info import ContestantItem, Contestant
from src.data.pay_load import DBG, ENTRY, DBGE, DETAIL, GleamPayload, GleamPayloadNoneDetails, \
    GleamPayloadCustomAction
from src.data.profile import ChromeProfile
from src.feature.auto_gleam.data_track_event.discord import JOIN_DISCORD_SERVER
from src.feature.auto_gleam.data_track_event.other import SUBMIT_URL, SECRET_CODE
from src.feature.auto_gleam.data_track_event.twitter import Twitter
from src.utils.web_element_handler import WebElementHandler


class RequestHandler:

    def __init__(self, profile: ChromeProfile, root_element: UCWebElement, web_element_handler: WebElementHandler):
        self.profile = profile
        self.root_element = root_element
        self.web_element_handler = web_element_handler

    def get_entry_method_id(self):
        return self.root_element.get_attribute("id").replace("em", "")

    def get_campaign_key(self):
        div = "//div[@class='campaign competition language-en ng-scope']"
        json_element: UCWebElement = self.web_element_handler.find_element(By.XPATH, div)
        campaign: str = json_element.get_attribute("ng-init")
        campaign = campaign.replace("initContestant(", "")
        pattern = "[)](; initEntryCount)[(].*[)]"
        campaign = re.sub(pattern, "", campaign)
        obj = json.loads(campaign)["contestant"]
        try:
            vsp: str = obj["viral_share_paths"]
            key = vsp.split(":")[0].replace("{", "").replace("\"", "")
        except:
            key = re.match("^(https:)(//)(gleam.io)(/)(.*)(/)", self.profile.url).group(5)
        print("key:  " + key)
        return key

    def get_entry_type(self, action: str):
        arr = action.replace("###APP_NAME### ", "").split("|")
        if len(arr) > 2:
            entry_type = "'" + arr[1] + "_" + arr[2] + "'"
        else:
            entry_type = "'" + arr[0] + "_" + arr[1] + "'"
        return entry_type

    def get_hash_h_value(self):
        div = "//div[@class='campaign competition language-en ng-scope']"
        json_element: UCWebElement = self.web_element_handler.find_element(By.XPATH, div)
        campaign: str = json_element.get_attribute("ng-init")
        campaign = campaign.replace("initContestant(", "")
        pattern = "[)](; initEntryCount)[(].*[)]"
        campaign = re.sub(pattern, "", campaign)
        obj = json.loads(campaign)["contestant"]
        contestant_id = ""
        try:
            contestant_id = "-" + str(obj["id"])
        except:
            print("failed to get contestant id")
        if self.profile.contestant_id > 0:
            contestant_id: str = "-" + str(self.profile.contestant_id)
        campaign_key: str = "'" + self.get_campaign_key() + "'"
        entry_method_id: str = "'" + self.get_entry_method_id() + "'"
        file_path: Path = Path(__file__).parent.parent.joinpath("../script/hash-h.txt")
        file = open(file_path, 'r', encoding='utf-8')
        script = file.read(). \
            replace("contestant.id", contestant_id). \
            replace("entry_method.id", entry_method_id). \
            replace("entry_method.entry_type", self.profile.entry_type). \
            replace("campaign.key", campaign_key)
        file.close()
        return self.web_element_handler.execute_script(script)

    def get_hash_f_value(self):
        file_path: Path = Path(__file__).parent.parent.joinpath("../script/hash-f.txt")
        file = open(file_path, 'r', encoding='utf-8')
        script = file.read().replace("ChromeUserAgent", self.profile.user_agent)
        file.close()
        return self.web_element_handler.execute_script(script)

    def get_pay_load(self):
        dbg = DBG()
        dbg.eds = {str(self.get_entry_method_id()): ENTRY(self.profile.twitter_user_name.replace("@", "")).__dict__}
        dbg.efd = {str(self.get_entry_method_id()): ENTRY(self.profile.twitter_user_name.replace("@", "")).__dict__}
        dbg.afd = {}
        dbg.car = True
        dbge = DBGE()
        dbge.eed = "7"
        dbge.hed = "#" + "entry_method_id" + ":undefined:undefined:undefined"
        dbge.hedr = "#" + "entry_method_id" + ":undefined:undefined:undefined"
        dbge.csefr = "rFalse"
        dbge.csefn = "#undefined:false"
        dbge.re = "sed"
        detail = DETAIL()
        detail.twitter_username = self.profile.twitter_user_name.replace("@", "")
        if self.profile.data_track_event == JOIN_DISCORD_SERVER or self.profile.data_track_event == Twitter.RETWEET:
            payload_none_details = GleamPayloadNoneDetails()
            payload_none_details.dbg = dbg.__dict__
            payload_none_details.dbge = dbge.__dict__
            payload_none_details.challenge_response = None
            payload_none_details.h = self.get_hash_h_value()
            payload_none_details.f = self.get_hash_f_value()
            payload_none_details.use_hcaptcha = False
            payload_none_details.rid = False
            return json.dumps(payload_none_details.__dict__)
        elif self.profile.is_custom_details:
            payload_custom = GleamPayloadCustomAction()
            payload_custom.dbg = dbg.__dict__
            payload_custom.dbge = dbge.__dict__
            payload_custom.challenge_response = None
            if self.profile.data_track_event == SUBMIT_URL:
                payload_custom.details = "https://twitter.com/home"
            elif self.profile.data_track_event == SECRET_CODE:
                payload_custom.details = self.profile.secret_code
            else:
                payload_custom.details = self.profile.wallet
            payload_custom.h = self.get_hash_h_value()
            payload_custom.f = self.get_hash_f_value()
            payload_custom.use_hcaptcha = False
            payload_custom.rid = False
            return json.dumps(payload_custom.__dict__)
        else:
            gleam_payload = GleamPayload()
            gleam_payload.dbg = dbg.__dict__
            gleam_payload.dbge = dbge.__dict__
            gleam_payload.challenge_response = None
            gleam_payload.details = detail.__dict__
            gleam_payload.h = self.get_hash_h_value()
            gleam_payload.f = self.get_hash_f_value()
            gleam_payload.use_hcaptcha = False
            gleam_payload.rid = False
            return json.dumps(gleam_payload.__dict__)

    def start_request(self, func):
        response = self.call_api()
        error = self.has_this_value(response.text, "error")
        if response.status_code == 200:
            if self.has_this_value(response.text, "worth") == 1:
                self.profile.total_completed_remaining_event += 1
            # click vào entry để authen lại các mạng xã hội
            elif error == "error_auth_expired":
                func[0](func[1], 'error_auth_expired')
                response2 = self.call_api()  # recall api to check
                if self.has_this_value(response2.text, "error") == "error_auth_expired":
                    func[0](func[1], 'error_auth_expired', True)
            elif error == "error_privacy" or error == "error_action_not_completed":
                func[0](func[1], None)
                self.call_api()  # recall api to check
            elif error == "not_logged_in":
                self.save_login_information()
                response = self.call_api()  # recall api to check
                if self.has_this_value(response.text, 'error_challenge_failed'):
                    func[0](func[1], 'error_challenge_failed', True)
                    response = self.call_api()  # recall api to check
                    if self.has_this_value(response.text, 'error_challenge_failed'):
                        func[0](func[1], 'close_entry')  # click lại entry
                        response = self.call_api()  # recall api to check
                        if self.has_this_value(response.text, 'error_challenge_failed'):
                            func[0](func[1], 'error_challenge_failed', True)
                elif self.has_this_value(response.text, 'error') == 'error_action_not_completed':
                    func[0](func[1], None)
                    self.call_api()  # recall api to check
            elif self.has_this_value(response.text, 'error_challenge_failed'):
                func[0](func[1], 'error_challenge_failed')
                response2 = self.call_api()  # recall api to check
                if self.has_this_value(response2.text, "error_challenge_failed"):
                    func[0](func[1], 'error_challenge_failed', True)
            # case này không nên xảy ra vì các event đã được sắp xếp để không bị khóa khi thực hiện tuần tự
            elif self.has_this_value(response.text, 'require_campaign_refresh":true') == 'true':
                self.profile.driver.refresh()
                self.call_api()  # recall api to check

        else:
            self.save_login_information()
            self.call_api()  # recall api to check

    def get_headers_and_proxy(self):
        headers = {
            "content-type": "application/json;charset=UTF-8",
            "cookie": self.web_element_handler.get_cookies(),
            "referer": self.profile.url,
            "user-agent": self.profile.user_agent,

        }
        raw_proxy = self.profile.proxy
        data = raw_proxy.split(":")
        user_name = data[3]
        pass_word = data[4]
        port = data[2]
        ip = data[1].replace('//', "")
        form = 'http://' + user_name + ':' + pass_word + '@' + ip + ':' + port
        proxy = {'http': form,
                 'https': form}

        return headers, proxy

    def save_login_information(self):
        headers, proxy = self.get_headers_and_proxy()
        contestant_item = ContestantItem()
        contestant_item.email = self.profile.gmail_address
        contestant_item.first_name = self.profile.gmail_address.split('@')[0]
        contestant_item.last_name = self.profile.gmail_address.split('@')[1]
        contestant_item.name = self.profile.gmail_address.split('@')[0]
        contestant_item.competition_subscription = None
        contestant_item.date_of_birth = '1996-12-06'
        contestant_item.stored_dob = '1996-12-06'
        contestant_item.bep20_wallet_address = self.profile.wallet
        contestant_item.erc20_address = self.profile.wallet
        contestant_item.twitter_account = self.profile.twitter_user_name
        contestant_item.telegram_account = self.profile.twitter_user_name.replace('@', '')
        contestant_item.send_confirmation = True
        contestant_item.email_verfication_required = False
        contestant = Contestant()
        contestant.contestant = contestant_item.__dict__
        contestant.additional_details = True
        contestant.campaign_key = self.get_campaign_key()
        post_url = 'https://gleam.io/set-contestant'
        return requests.post(url=post_url, json=json.loads(json.dumps(contestant.__dict__)), headers=headers,
                             proxies=proxy)

    def call_api(self):
        headers, proxy = self.get_headers_and_proxy()
        post_url = "https://gleam.io/enter/" + self.get_campaign_key() + "/" + self.get_entry_method_id()
        payload = self.get_pay_load()
        response = requests.post(url=post_url, json=json.loads(payload), headers=headers, proxies=proxy)
        print(response.text)
        return response

    def has_this_value(self, response: str, key: str):
        try:
            return json.loads(response)[key]
        except:
            return None
