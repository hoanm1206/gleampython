import json

import undetected_chromedriver as uc

from src.data.profile import ChromeProfile
from src.feature.auto_gleam.domain.usecase.entry_handler import EntryHandler
from src.feature.auto_gleam.domain.usecase.event_finder import EventFinder
from src.start_gologin import Product
from src.utils.multiple_thread_chrome_handler import *
from src.utils.web_element_handler import WebElementHandler


class ChromeDriver:

    # init the list profile chrome for reused
    def __init__(self, list_profile_chrome: list[ChromeProfile]):
        self.list_profile_chrome = list_profile_chrome

    # define index of the Chrome on the screen, start from 0 -> 5, call this before calculate_browser_coordinate_position()
    def __calculate_browser_index(self):
        current_index = 0
        for profile in self.list_profile_chrome:
            if profile.selected:
                profile.current_index = current_index
                if current_index < 6:
                    current_index += 1
                else:
                    current_index = 0

    # default using 6 Chrome window,define coordinate position for each Chrome and chrome size by percentage
    def __calculate_browser_coordinate_position(self):
        for profile in self.list_profile_chrome:
            if profile.current_index in range(3):
                profile.y_position = 0
                profile.x_position = profile.current_index
            elif profile.current_index in range(3, 6):
                profile.y_position = 1
                profile.x_position = profile.current_index - 3
            profile.chrome_height = 2
            profile.chrome_width = 3

    def __init_browser_window(self, profile: ChromeProfile):
        starts = Product({
            "tmpdir": 'D:\\Airdrop\\Profile',  # Đường dẫn folder profile
            "local": False,
            "credentials_enable_service": False,
            "user_agent": profile.user_agent
        })
        proxy = profile.proxy
        # proxy = 'socks5://45.152.225.108:64711:EAzGMRSu:ZiNQDnXh'
        # proxy = 'socks5://tam.1proxy.net:46553:test:1proxy'
        profile_id = profile.profile_id
        browser_path = starts.executablePath
        chrome_driver_path = 'C:\\Users\\hoanm\\AppData\\Roaming\\undetected_chromedriver\\undetected_chromedriver.exe'

        starts.setProfileId(profile_id)
        if proxy:
            starts.Change_Proxy(proxy)  # Đổi proxy cho profile cần mở. Để cảm bảo vân tay chính xác theo proxy
            starts.update(profile_id)  # Cập nhật profile

        print('Profile start: ', profile_id)
        print('Proxy start: ', proxy)
        params = starts.getParams(proxy)  # Kết nối với profile

        options = uc.ChromeOptions()
        for param in params:
            options.add_argument(param)
        driver = uc.Chrome(
            version_main=111,
            use_subprocess=True,
            driver_executable_path=chrome_driver_path,
            browser_executable_path=browser_path,
            options=options)
        if len(profile.chrome_size) == 0:
            driver.maximize_window()
            profile.chrome_size = driver.get_window_size()
        desired_width = profile.chrome_size["width"] / profile.chrome_width
        desired_height = profile.chrome_size["height"] / profile.chrome_height
        driver.set_window_size(desired_width, desired_height)
        driver.set_window_position(desired_width * profile.x_position, desired_height * profile.y_position)
        url = 'https://gleam.io/zWCCP/15000-worth-of-gptg-giveaway'
        profile.url = url
        driver.get(url)
        profile.driver = driver
        driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        # init element factory
        element_handler = WebElementHandler(driver)
        # find events by element
        event_finder = EventFinder(profile, element_handler)
        list_root_element = event_finder.get_all_root_element()
        list_event = event_finder.get_all_event_remaining()
        # get point
        entry_handler = EntryHandler(profile, list_root_element, list_event, element_handler)
        entry_handler.make_events()
        driver.close()

    def show_browsers_on_screen(self):
        self.__calculate_browser_index()
        self.__calculate_browser_coordinate_position()
        for index in range(1):
            list_item = (self.__init_browser_window, self.list_profile_chrome[0])
            push_task_to_queue(list_item, 6)
        start_queue()

# if __name__ == '__main__':
#     ChromeDriver(get_list_profile_chrome()).show_browsers_on_screen()
