import time

from selenium.webdriver.common.by import By

from src.data.profile import ChromeProfile
from src.utils.web_element_handler import WebElementHandler


def join_discord_server(profile: ChromeProfile, web_element_handler: WebElementHandler):
    web_element_handler.open_new_tab_flow(profile)
    xpath = "//div[contains(text(),'Chấp nhận lời mời') or contains(text(),'Accept Invite')]"
    accept_button = web_element_handler.find_element(By.XPATH, xpath)
    if web_element_handler.try_click_button(accept_button):
        time.sleep(5)  # đợi join discord thành công
        web_element_handler.wait_for_page_load_complete()
        web_element_handler.close_new_tab_and_back_to_main_task()
