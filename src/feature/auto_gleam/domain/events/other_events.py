from selenium.webdriver.common.by import By
from undetected_chromedriver import UCWebElement

from src.utils.web_element_handler import WebElementHandler


def click_event_author_expired(web_element_handler: WebElementHandler, root_element: UCWebElement):
    xpath = ".//span[@class='tally']"
    entry_element = web_element_handler.find_element(By.XPATH, xpath, root_element)
    web_element_handler.try_click_button(entry_element)


def click_close_entry_button(web_element_handler: WebElementHandler, root_element: UCWebElement):
    xpath = ".//i[@class='fas fa-chevron-down']"
    entry_element = web_element_handler.find_element(By.XPATH, xpath, root_element)
    web_element_handler.try_click_button(entry_element)


def authentication_social(web_element_handler: WebElementHandler, root_element: UCWebElement):
    xpath = ".//*[@ng-click='openAuthentication(entry_method.provider)']"
    entry_element = web_element_handler.find_element(By.XPATH, xpath, root_element)
    web_element_handler.execute_script("$(arguments[0]).click();", entry_element)
    web_element_handler.try_click_button(entry_element)
