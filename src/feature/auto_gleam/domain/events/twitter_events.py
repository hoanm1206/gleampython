from selenium.webdriver.common.by import By

from src.data.profile import ChromeProfile
from src.utils.web_element_handler import WebElementHandler


def tweet_with_hash_tag(profile: ChromeProfile, web_element_handler: WebElementHandler):
    web_element_handler.open_new_tab_flow(profile)
    tweet_button1 = web_element_handler.find_element(By.XPATH, "//div[@dir='auto']//descendant::*[text() ='Tweet']")
    tweet_button2 = web_element_handler.find_element(By.XPATH, "//div[@data-testid='tweetButton']//descendant::*[text() ='Tweet']")
    if web_element_handler.try_click_button(tweet_button1) or web_element_handler.try_click_button(tweet_button2):
        web_element_handler.close_new_tab_and_back_to_main_task()


def add_tweet_hash_tag(profile: ChromeProfile, web_element_handler: WebElementHandler):
    web_element_handler.open_new_tab_flow(profile)
    add_tweet = web_element_handler.find_element(By.XPATH, "//div[@aria-label='Add Tweet']//following::div[1]")
    if web_element_handler.try_click_button(add_tweet):
        web_element_handler.close_new_tab_and_back_to_main_task()


def follow_twitter(profile: ChromeProfile, web_element_handler: WebElementHandler):
    web_element_handler.open_new_tab_flow(profile)
    xpath = "//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Follow' or text()='Theo dõi']"
    follow_button = web_element_handler.find_element(By.XPATH, xpath)
    if web_element_handler.try_click_button(follow_button):
        web_element_handler.close_new_tab_and_back_to_main_task()


def retweet_twitter(profile: ChromeProfile, web_element_handler: WebElementHandler):
    web_element_handler.open_new_tab_flow(profile)
    xpath = "//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Retweet']"
    retweet_button = web_element_handler.find_element(By.XPATH, xpath)
    if web_element_handler.try_click_button(retweet_button):
        web_element_handler.close_new_tab_and_back_to_main_task()
