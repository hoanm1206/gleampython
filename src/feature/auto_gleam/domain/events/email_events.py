from selenium.webdriver.common.by import By
from undetected_chromedriver.webelement import UCWebElement

from src.data.profile import ChromeProfile
from src.utils.web_element_handler import WebElementHandler


def subscribe_email(web_element_handler: WebElementHandler, root_element: UCWebElement):
    xpath1 = ".//*[@class='checkbox']"
    check_box = web_element_handler.find_element(By.XPATH, xpath1, root_element)
    if web_element_handler.try_click_button(check_box):
        xpath2 = ".//*[@ng-click='saveEntryDetails(entry_method)']"
        entry_button = web_element_handler.find_elements(By.XPATH, xpath2)
        web_element_handler.try_click_button(entry_button)
