import os

from src.feature.auto_gleam.domain.usecase.undetected_chrome_driver_impl import ChromeDriver
from src.repository.local.chrome_profile_database import get_list_profile_chrome


class GleamViewModel:

    def __init__(self):
        # show browser on screen
        self.list_profile_chrome = get_list_profile_chrome()

    def start(self):
        self.show_browsers_on_screen()

    def show_browsers_on_screen(self):
        driver = ChromeDriver(get_list_profile_chrome())
        driver.show_browsers_on_screen()

    # def resolve_entry_methods(self):
    #     event_finder = EventFinder()


def re_format_file():
    file = open('D:\\Airdrop\\Accounts\\twitter.txt', 'r')
    file2 = open('D:\\Airdrop\\Accounts\\Test.txt', 'w')
    data = file.readlines()
    lst = list()
    for item in range(0, len(data)):
        value = str(item + 1) + ": " + data[item] + "\n"
        lst.append(value)
        file2.writelines(value)


def change_profiles_folder_name():
    # define the path of the directory
    dir_path = "D:\\Airdrop\\Profile6.0.1"

    # use the os.listdir() method to get a list of all files and folders in the directory
    files_and_folders = os.listdir(dir_path)

    # loop through the list and filter out only the folders
    folders = [f for f in files_and_folders if os.path.isdir(os.path.join(dir_path, f))]

    # print the list of folders
    print(folders)
    for item in range(0, 100):
        old_path = os.path.join(dir_path, folders[item])
        new_path = os.path.join(dir_path, str(item+1))
        os.rename(old_path, new_path)


if __name__ == '__main__':
    #change_profiles_folder_name()
    GleamViewModel().start()
