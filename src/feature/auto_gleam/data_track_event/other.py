SUBMIT_URL = "###APP_NAME### Click|submit|url"

SHARE_ACTION = "###APP_NAME### Click|share|action"

SECRET_CODE = "###APP_NAME### Click|secret|code"

SUBSCRIBE_EMAIL = "###APP_NAME### Click|email|subscribe"

LOYALTY = "###APP_NAME### Click|loyalty|loyalty"

WALLET_ADDRESS = "###APP_NAME### Click|wallet|address"

CUSTOM_ACTION = "###APP_NAME### Click|custom|action"
