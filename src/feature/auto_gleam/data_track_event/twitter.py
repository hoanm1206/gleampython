class Twitter:
    FOLLOW = "###APP_NAME### Click|twitter|follow"

    RETWEET = "###APP_NAME### Click|twitter|retweet"

    TWEET = "###APP_NAME### Click|twitter|tweet"

    TWEET_WITH_HASH_TAG = "###APP_NAME### Click|twitter|hashtags"

    ENTER = "###APP_NAME### Click|twitter|enter"

    VIEW_POST = "###APP_NAME### Click|twitter|view_post"

    SELECT_PHOTO = "###APP_NAME### Click|twitter|media"
